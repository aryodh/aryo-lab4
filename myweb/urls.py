"""lab4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views

app_name = 'myweb'
urlpatterns = [
    path('', views.index, name='index'),
    path('portofolio', views.portofolio, name='portofolio'),
    path('about', views.about, name='about'),
    path('message', views.message, name='message'),
    path('login', views.login, name='login'),
    path('signup', views.signup, name='signup'),
    path('create_sched', views.create_sched, name = 'create_sched'),
    path('sched_post', views.sched_post, name = 'sched_post'),
    path('view_sched', views.view_sched, name = 'view_sched'),
    path('delete_all_sched_db', views.delete_all_sched_db, name = 'delete_all_sched_db'),
]
