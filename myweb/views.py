from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Sched_form
from .models import Schedule

# Create your views here.
def default_response():
    response = {
        'title':'',
        'homstat':'',
        'porstat':'',
        'abtstat':'',
        'msgstat':'',
        'scdstat':'',
    }
    return response

def index(request):
    response = default_response()
    response['title'] = 'Home'
    response['homstat'] = 'active-self'
    return render(request, 'index.html', response)


def portofolio(request):
    response = default_response()
    response['title'] = 'Portofolio'
    response['porstat'] = 'active-self'
    return render(request, 'portofolio.html', response)


def about(request):
    response = default_response()
    response['title'] = 'About'
    response['abtstat'] = 'active-self'
    return render(request, 'about.html', response)


def message(request):
    response = default_response()
    response['title'] = 'Message'
    response['msgstat'] = 'active-self'
    return render(request, 'message.html', response)


def login(request):
    response = {}
    return render(request, 'login.html', response)


def signup(request):
    response = {}
    return render(request, 'register.html', response)

def create_sched(request):
    response = {}
    response['title'] = 'Create Schedule'
    response['schedule_form'] = Sched_form
    return render(request, 'create_sched.html', response)

def sched_post(request):
    response = {}
    form = Sched_form(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        response['event_name'] = request.POST['event_name']
        response['event_date'] = request.POST['event_date']
        response['event_time'] = request.POST['event_time']
        response['event_category'] = request.POST['event_category']
        response['event_place'] = request.POST['event_place']
        
        schedule = Schedule(
            event_name = response['event_name'], 
            event_date = response['event_date'],
            event_time = response['event_time'],
            event_category = response['event_category'],
            event_place = response['event_place']
        )
        schedule.save()
        response1 = {}
        schedule = Schedule.objects.all()
        response1['schedule'] = schedule
        response1['showpopup'] = 'visibilty: block'
        response1['word'] = "Forms has been updated!"
        html = 'view_sched.html'
        return render(request, html, response1)
    
    else:
        return HttpResponseRedirect('/')

def view_sched(request):
    response = {}
    schedule = Schedule.objects.all()
    response['title'] = 'Schedule'
    response['schedule'] = schedule
    response['showpopup'] = 'visibility: hidden'
    html = 'view_sched.html'
    return render(request, html , response)


def delete_all_sched_db(request):
    Schedule.objects.all().delete()
    response = {}
    response['title'] = 'Schedule'
    response['showpopup'] = 'visibilty: block'
    response['word'] = "Forms has been deleted!"
    html = 'view_sched.html'
    return render(request, html, response)