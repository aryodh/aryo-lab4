from django import forms

class Sched_form(forms.Form):
    error_msg = {
        'required' : 'Please fill the blank section',
        'invalid' : 'Please fill correctly'
    }

    attrs = {
        'class' : 'form-control'
    }

    event_name = forms.CharField(
        label = 'Event Name',
        max_length = 30,
        widget = forms.TextInput(attrs = attrs)
    )
    
    event_date = forms.DateField(
        label = 'Date',
        widget = forms.DateInput(attrs = {'type':'date'})
    )

    event_time = forms.TimeField(
        label = 'Time',
        widget = forms.TimeInput(attrs = {'type':'time'})
    )

    event_category = forms.CharField(
        label = 'category',
        max_length = 30,
        widget = forms.TextInput(attrs = attrs)
    )

    event_place = forms.CharField(
        label = 'Place',
        max_length = 30,
        widget = forms.TextInput(attrs = attrs)
    )