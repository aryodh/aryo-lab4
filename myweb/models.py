from django.db import models
from django.utils import timezone


class Schedule(models.Model):
    event_name = models.CharField(max_length = 30)
    event_date = models.DateField()
    event_time = models.TimeField()
    event_category = models.CharField(max_length = 30)
    event_place = models.CharField(max_length = 30)
    created_date = models.DateTimeField(default = timezone.now)

    def __str__(self):
        return 'Event Name : ' + self.event_name